#pragma once
#include <string>
#include <random>
#include <sstream>
#include <map>
#include <type_traits>
#include <algorithm>
#include "board2584.h"
#include "action2584.h"

using namespace std;

class agent {
public:
	agent(const std::string& args = "") {
		std::stringstream ss(args);
		for (std::string pair; ss >> pair; ) {
			std::string key = pair.substr(0, pair.find('='));
			std::string value = pair.substr(pair.find('=') + 1);
			property[key] = { value };
		}
	}
	virtual ~agent() {}
	virtual void open_episode(const std::string& flag = "") {}
	virtual void close_episode(const std::string& flag = "") {}
	virtual action take_action(const board& b) { return action(); }
	virtual bool check_for_win(const board& b) { return false; }

public:
	virtual std::string name() const {
		auto it = property.find("name");
		return it != property.end() ? std::string(it->second) : "unknown";
	}
protected:
	typedef std::string key;
	struct value {
		std::string value;
		operator std::string() const { return value; }
		template<typename numeric, typename = typename std::enable_if<std::is_arithmetic<numeric>::value, numeric>::type>
		operator numeric() const { return numeric(std::stod(value)); }
	};
	std::map<key, value> property;
};

/**
* evil (environment agent)
* add a new random tile on board, or do nothing if the board is full
* 1-tile: 90%
* 2-tile: 10%
*/
class rndenv : public agent {
public:
	rndenv(const std::string& args = "") : agent("name=rndenv " + args) {
		if (property.find("seed") != property.end())
			engine.seed(int(property["seed"]));
	}

	virtual action take_action(const board& after) {
		int space[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
		std::shuffle(space, space + 16, engine);
		for (int pos : space) {
			if (after(pos) != 0) continue;
			std::uniform_int_distribution<int> popup(0, 9);
			int tile = popup(engine) ? 1 : 2;
			return action::place(tile, pos);
		}
		return action();
	}

private:
	std::default_random_engine engine;
};

/**
* player (dummy)
* select an action randomly
*/
class player : public agent {
public:
	player(const std::string& args = "") : agent("name=player " + args) 
	{
		if (property.find("seed") != property.end())
			engine.seed(int(property["seed"]));
	}

	virtual action take_action(const board& before) {
		float maxValue = 0;
		int maxAction = -1;
		for (int i = 0; i < 4; i++)
		{
			board b = before;
			float value = b.move(i);
			if (value == -1)
				continue;
			for (int j = 0; j < 16; j++)
			{
				value += Fibonacci2584Table[b(j)] * valueTable[j];
			}
			b.reflect_horizontal();
			for (int j = 0; j < 16; j++)
			{
				value += Fibonacci2584Table[b(j)] * valueTable[j];
			}
			
			if (value >= maxValue)
			{
				maxValue = value;
				maxAction = i;
			}
		}
		if(maxAction == -1)
			return action();
		else
			return action(maxAction);
	}

private:
	std::default_random_engine engine;
	float valueTable[16]
	{
		1, 0.5, 0.25, 0.125,
		0.5, 0.25, 0.125, 0.0625,
		0.25, 0.125, 0.0625, 0.03125,
		0.125, 0.0625, 0.03125, 0.015625
	};
};
