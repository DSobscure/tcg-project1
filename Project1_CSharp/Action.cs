﻿namespace Project1_CSharp
{
    public struct Action
    {
        public static Action Move(int oper)
        {
            return new Action(oper);
        }
        public static Action Place(int tile, int pos)
        {
            return new Action((tile << 4) | (pos));
        }

        public readonly int opcode;

        public Action(Action act)
        {
            opcode = act.opcode;
        }

        public Action(int op = -1)
        {
            opcode = op;
        }

        public override string ToString()
        {
            if ((0b11 & opcode) == (opcode))
            {
                string[] opname = { "up", "right", "down", "left" };
                return "slide " + opname[opcode];
            }
            else
            {
                return $"place {opcode >> 4}-index at position {opcode & 0x0f}";
            }
        }

        public int Apply(Board b)
        {
		    if ((0b11 & opcode) == (opcode))
            {
			    // player action (slide up, right, down, left)
			    return b.Move(opcode);
		    }
		    else if (b[opcode & 0x0f] == 0)
            {
                // environment action (place a new tile)
                b[opcode & 0x0f] = (opcode >> 4);
			    return 0;
		    }
		    return -1;
	    }
    }
}
