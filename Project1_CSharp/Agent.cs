﻿using System.Collections.Generic;

namespace Project1_CSharp
{
    public abstract class Agent
    {
        protected Dictionary<string, object> property = new Dictionary<string, object>();

        protected Agent(string args = "")
        {
            foreach(var item in args.Split(' '))
            {
                if (item.Length < 1)
                    continue;
                string key = item.Substring(0, item.IndexOf('='));
                string value = item.Substring(item.IndexOf('=') + 1);
                property.Add(key, value);
            }
        }

        public override string ToString()
        {
            if (property.ContainsKey("name"))
            {
                return property["name"].ToString();
            }
            else
            {
                return "unknown";
            }
        }

        public virtual void OpenEpisode(string flag = "")
        {
        }
	    public virtual void CloseEpisode(string flag = "")
        {
        }
        public abstract Action TakeAction(Board board);
        public virtual bool CheckForWin(Board b)
        {
            return false;
        }
    }
}
