﻿using System.Text;

namespace Project1_CSharp
{
    public unsafe class Board
    {
        private static int[,,,][] shiftLeftTable = new int[33,33,33,33][];
        private static int[,,,] shiftLeftRewardTable = new int[33, 33, 33, 33];
        private static bool[,,,] shiftLeftModifiedTable = new bool[33, 33, 33, 33];
        public static void InitialTable()
        {
            for(int c0 = 0; c0 < 33; c0++)
            {
                for (int c1 = 0; c1 < 33; c1++)
                {
                    for (int c2 = 0; c2 < 33; c2++)
                    {
                        for (int c3 = 0; c3 < 33; c3++)
                        {
                            shiftLeftTable[c0, c1, c2, c3] = new int[4] { c0, c1, c2, c3 };
                            int score = 0;
                            int top = 0, hold = 0;
                            for (int c = 0; c < 4; c++)
                            {
                                int tile = shiftLeftTable[c0, c1, c2, c3][c];
                                if (tile == 0) continue;
                                shiftLeftTable[c0, c1, c2, c3][c] = 0;
                                if (hold != 0)
                                {
                                    if ((tile == 1 && tile == hold) || (tile == hold + 1))
                                    {
                                        tile += 1;
                                        shiftLeftTable[c0, c1, c2, c3][top++] = tile;
                                        score += Fibonacci2584Table.values[tile];
                                        hold = 0;
                                    }
                                    else if (tile == hold - 1)
                                    {
                                        tile += 2;
                                        shiftLeftTable[c0, c1, c2, c3][top++] = tile;
                                        score += Fibonacci2584Table.values[tile];
                                        hold = 0;
                                    }
                                    else
                                    {
                                        shiftLeftTable[c0, c1, c2, c3][top++] = hold;
                                        hold = tile;
                                    }
                                }
                                else
                                {
                                    hold = tile;
                                }
                            }
                            if (hold != 0)
                                shiftLeftTable[c0, c1, c2, c3][top] = hold;
                            shiftLeftRewardTable[c0, c1, c2, c3] = score;

                            if(shiftLeftTable[c0, c1, c2, c3][0] != c0 ||
                                shiftLeftTable[c0, c1, c2, c3][1] != c1 ||
                                shiftLeftTable[c0, c1, c2, c3][2] != c2 ||
                                shiftLeftTable[c0, c1, c2, c3][3] != c3)
                            {
                                shiftLeftModifiedTable[c0, c1, c2, c3] = true;
                            }

                        }
                    }
                }
            }
        }

        private int[] tiles = new int[16];


        public int EmptyCount
        {
            get
            {
                int result = 0;
                for (int i = 0; i < 16; i++)
                {
                    if (this[i] == 0)
                        result++;
                }
                return result;
            }
        }

        public Board()
        {

        }
        public Board(Board board)
        {
            for(int i = 0; i < 16; i++)
            {
                tiles[i] = board.tiles[i];
            }
        }

        public int this[int key]
        {
            get
            {
                return tiles[key];
            }
            set
            {
                tiles[key] = value;
            }
        }

        public int this[int key1, int key2]
        {
            get
            {
                return this[4 * key1 + key2];
            }
            set
            {
                this[4 * key1 + key2] = value;
            }
        }

        public bool Equals(Board board)
        {
            for (int i = 0; i < 16; i++)
            {
                if (this[i] != board[i])
                {
                    return false;
                }
            }
            return true;
        }

        public override string ToString()
        {
            StringBuilder outputBuilder = new StringBuilder();
            outputBuilder.AppendLine("+------------------------+");

            for (int i = 0; i < 4; i++)
            {
                outputBuilder.AppendFormat("|{0,6}{1,6}{2,6}{3,6}|\n", 
                    Fibonacci2584Table.values[this[i, 0]],
                    Fibonacci2584Table.values[this[i, 1]],
                    Fibonacci2584Table.values[this[i, 2]],
                    Fibonacci2584Table.values[this[i, 3]]);
            }
            outputBuilder.AppendLine("+------------------------+");

            return outputBuilder.ToString();
        }

        public void Transpose()
        {
            for (int r = 0; r < 4; r++)
            {
                for (int c = r + 1; c < 4; c++)
                {
                    int t = this[r, c];
                    this[r, c] = this[c, r];
                    this[c, r] = t;
                }
            }
        }
        public void ReflectHorizontal()
        {
            for (int r = 0; r < 4; r++)
            {
                int t = this[r, 0];
                this[r, 0] = this[r, 3];
                this[r, 3] = t;

                t = this[r, 1];
                this[r, 1] = this[r, 2];
                this[r, 2] = t;
            }
        }
        public void ReflectVertical()
        {
            for (int c = 0; c < 4; c++)
            {
                int t = this[0, c];
                this[0, c] = this[3, c];
                this[3, c] = t;

                t = this[1, c];
                this[1, c] = this[2, c];
                this[2, c] = t;
            }
        }

        public void Rotate(int r = 1)
        {
		    switch (((r % 4) + 4) % 4)
            {
                case 0:
                    break;
		        case 1:
                    RotateRight();
                    break;
		        case 2:
                    Reverse();
                    break;
		        case 3:
                    RotateLeft();
                    break;
                default:
                    break;
            }
        }
        private void RotateRight()
        {
            Transpose();
            ReflectHorizontal();
        }
        private void RotateLeft()
        {
            Transpose();
            ReflectVertical();
        }
        private void Reverse()
        {
            ReflectHorizontal();
            ReflectVertical();
        }

        public int Move(int opcode)
        {
		    switch (opcode)
            {
		        case 0:
                    return MoveUp();
		        case 1:
                    return MoveRight();
		        case 2:
                    return MoveDown();
		        case 3:
                    return MoveLeft();
		        default:
                    return -1;
		    }
        }

        private unsafe int MoveLeft()
        {
            bool modified = false;
            int score = 0;

            for (int r = 0; r < 4; r++)
            {
                int[] afterRow = shiftLeftTable[this[r, 0], this[r, 1], this[r, 2], this[r, 3]];
                score += shiftLeftRewardTable[this[r, 0], this[r, 1], this[r, 2], this[r, 3]];
                if(!modified)
                    modified = shiftLeftModifiedTable[this[r, 0], this[r, 1], this[r, 2], this[r, 3]];
                for(int i = 0; i < 4; i++)
                    this[r, i] = afterRow[i];
                //int top = 0, hold = 0;
                //for (int c = 0; c < 4; c++)
                //{
                //    originalRow[c] = this[r, c];
                //    int tile = this[r, c];
                //    if (tile == 0) continue;
                //    this[r, c] = 0;
                //    if (hold != 0)
                //    {
                //        if ((tile == 1 && tile == hold) || (tile == hold + 1))
                //        {
                //            tile += 1;
                //            this[r, top++] = tile;
                //            score += Fibonacci2584Table.values[tile];
                //            hold = 0;
                //        }
                //        else if (tile == hold - 1)
                //        {
                //            tile += 2;
                //            this[r, top++] = tile;
                //            score += Fibonacci2584Table.values[tile];
                //            hold = 0;
                //        }
                //        else
                //        {
                //            this[r, top++] = hold;
                //            hold = tile;
                //        }
                //    }
                //    else
                //    {
                //        hold = tile;
                //    }
                //}
                //if (hold != 0)
                //    this[r,top] = hold;
                //if(!modified)
                //{
                //    for (int c = 0; c < 4; c++)
                //    {
                //        if (originalRow[c] != this[r, c])
                //        {
                //            modified = true;
                //            break;
                //        }
                //    }
                //}
            }
            return (modified) ? score : -1;
        }
        private int MoveRight()
        {
            ReflectHorizontal();
            int score = MoveLeft();
            ReflectHorizontal();
            return score;
        }
        private int MoveUp()
        {
            RotateRight();
            int score = MoveRight();
            RotateLeft();
            return score;
        }
        private int MoveDown()
        {
            RotateRight();
            int score = MoveLeft();
            RotateLeft();
            return score;
        }
    }
}
