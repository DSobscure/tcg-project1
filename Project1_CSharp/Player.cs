﻿using System;

namespace Project1_CSharp
{
    public class Player : Agent
    {
        private Random engine;
        private float[] valueTable = new float[]
	    {
		    1, 0.5f, 0.25f, 0.125f,
		    0.5f, 0.25f, 0.125f, 0.0625f,
		    0.25f, 0.125f, 0.0625f, 0.03125f,
		    0.125f, 0.0625f, 0.03125f, 0.015625f
        };

        public Player(string args = "") : base("name=player " + args)
        {
            if (property.ContainsKey("seed"))
            {
                engine = new Random((int)property["seed"]);
            }
            else
            {
                engine = new Random();
            }
        }

        public override Action TakeAction(Board board)
        {
            float maxValue = 0;
            int maxAction = -1;
            for (int i = 0; i < 4; i++)
            {
                Board b = new Board(board);
                float value = b.Move(i);
                if (value == -1)
                    continue;
                for (int j = 0; j < 16; j++)
                {
                    value += Fibonacci2584Table.values[b[j]] * valueTable[j];
                }
                b.ReflectHorizontal();
                for (int j = 0; j < 16; j++)
                {
                    value += Fibonacci2584Table.values[b[j]] * valueTable[j];
                }

                if (value >= maxValue)
                {
                    maxValue = value;
                    maxAction = i;
                }
            }
            if (maxAction == -1)
                return new Action();
            else
                return new Action(maxAction);
        }
    }
}
