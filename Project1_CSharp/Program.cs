﻿using System;
using System.IO;

namespace Project1_CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Board.InitialTable();
            Console.WriteLine("2584-Demo: ");
            Console.WriteLine();

            int total = 1000, block = 0;
            string play_args = null, evil_args = null;
            string load = null, save = null;
            bool summary = false;
            foreach(string para in args)
            {
                switch(para)
                {
                    case "--total=":
                        total = int.Parse(para.Substring(para.IndexOf('=') + 1));
                        break;
                    case "--block=":
                        block = int.Parse(para.Substring(para.IndexOf('=') + 1));
                        break;
                    case "--play=":
                        play_args = para.Substring(para.IndexOf('=') + 1);
                        break;
                    case "--evil=":
                        evil_args = para.Substring(para.IndexOf('=') + 1);
                        break;
                    case "--load=":
                        load = para.Substring(para.IndexOf('=') + 1);
                        break;
                    case "--save=":
                        save = para.Substring(para.IndexOf('=') + 1);
                        break;
                    case "--summary":
                        summary = true;
                        break;
                }
            }

            Statistic stat = new Statistic(total, block);

            if (load != null)
            {
                stat.Load(load);
            }

            Player play = new Player(play_args);
            RandomEnvironment evil = new RandomEnvironment(evil_args);

            while (!stat.IsFinished)
            {
                play.OpenEpisode("~:" + evil.ToString());
                evil.OpenEpisode(play.ToString() + ":~");

                stat.OpenEpisode(play.ToString() + ":" + evil.ToString());
                Board game = stat.MakeEmptyBoard();
                while (true)
                {
                    Agent who = stat.TakeTurns(play, evil);
                    Action move = who.TakeAction(game);
                    if (move.Apply(game) == -1)
                        break;
                    stat.SaveAction(move);
                    if (who.CheckForWin(game))
                        break;
                }
                Agent win = stat.LastTurns(play, evil);
                stat.CloseEpisode(win.ToString());

                play.CloseEpisode(win.ToString());
                evil.CloseEpisode(win.ToString());
            }

            if (summary)
            {
                stat.Summary();
            }

            if (save != null)
            {
                File.WriteAllText(save, stat.ToString());
            }

            Console.ReadLine();
        }
    }
}
