﻿using System;

namespace Project1_CSharp
{
    public class RandomEnvironment : Agent
    {
        private Random engine;

        public RandomEnvironment(string args = "") : base("name=rndenv " + args)
        {
            if (property.ContainsKey("seed"))
            {
                engine = new Random((int)property["seed"]);
            }
            else
            {
                engine = new Random();
            }
        }

        public override bool CheckForWin(Board b)
        {
            return false;
        }

        public override Action TakeAction(Board board)
        {
            int emptyCountCache = board.EmptyCount;
            if (emptyCountCache > 0)
            {
                int tile = (engine.Next(10) == 0) ? 2 : 1;

                int targetIndex = engine.Next(emptyCountCache);
                for (int i = 0; i < 16; i++)
                {
                    if (board[i] == 0)
                    {
                        if (targetIndex == 0)
                        {
                            return Action.Place(tile, i);
                        }
                        else
                        {
                            targetIndex--;
                        }
                    }
                }
            }
            return new Action();
        }
    }
}
