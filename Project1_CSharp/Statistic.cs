﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project1_CSharp
{
    public class Statistic
    {
        private class Record : List<Action>
        {
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }

        private int total;
        private int block;
        private List<Record> data = new List<Record>();

        public bool IsFinished
        {
            get
            {
                return data.Count >= total;
            }
        }

        public Statistic(int total, int block = 0)
        {
            this.total = total;
            this.block = (block != 0) ? block : total;
        }

        public void Show()
        {
            int block = Math.Min(data.Count, this.block);
            int sum = 0, max = 0, opc = 0;
            int[] stat = new int[33];
            TimeSpan duration = TimeSpan.Zero;
            for (int i = 0; i < block; i++)
            {
                Record path = data[data.Count - 1 - i];
                Board game = new Board();
                int score = 0;
                foreach (Action move in path)
                    score += move.Apply(game);
                sum += score;
                max = Math.Max(score, max);
                opc += (path.Count - 2) / 2;
                int tile = 0;
                for (int j = 0; j < 16; j++)
                    tile = Math.Max(tile, game[j]);
                stat[tile]++;
                duration += (path.EndTime - path.StartTime);
            }
            float avg = sum / (float)block;
            float coef = 100f / block;
            float ops = opc * 1000f / (float)duration.TotalMilliseconds;
            Console.WriteLine($"{data.Count}\tavg = {(int)avg}, max = {max}, ops = {(int)ops}");
            for (int t = 0, c = 0; c < block; c += stat[t++])
            {
                if (stat[t] == 0) continue;
                int accu = 0;
                for(int i = t; i < 33; i++)
                {
                    accu += stat[i];
                }
                Console.WriteLine($"\t{Fibonacci2584Table.values[t]}\t{accu * coef}%\t({stat[t] * coef}%)");
            }
            Console.WriteLine();
        }

        public void Summary()
        {
            int blockTemp = block;
            block = data.Count;
            Show();
            block = blockTemp;
        }

        public void OpenEpisode(string flag = "")
        {
            data.Add(new Record());
            data[data.Count - 1].StartTime = DateTime.Now;
        }

        public void CloseEpisode(string flag = "")
        {
            data[data.Count - 1].EndTime = DateTime.Now; ;
            if (data.Count % block == 0)
                Show();
        }

        public Board MakeEmptyBoard()
        {
            return new Board();
        }

        public void SaveAction(Action move)
        {
            data[data.Count - 1].Add(move);
        }

        public Agent TakeTurns(Agent play, Agent evil)
        {
            return (Math.Max(data[data.Count - 1].Count + 1, 2) % 2 != 0) ? play : evil;
        }

        public Agent LastTurns(Agent play, Agent evil)
        {
            return TakeTurns(evil, play);
        }


        public override string ToString()
        {
            StringBuilder recordBuilder = new StringBuilder();
            foreach(var rec in data)
            {
                recordBuilder.Append(rec);
            }
            return recordBuilder.ToString();
        }

        public void Load(string pathName)
        {

        }

        //friend std::istream& operator >>(std::istream& in, statistic& stat)
        //{
        //    auto size = stat.data.size();
        //  in.read(reinterpret_cast<char*>(&size), sizeof(size));
        //    stat.total = stat.block = size;
        //    stat.data.resize(size);
        //    for (record & rec : stat.data) in >> rec;
        //    return in;
        //}
    }
}
